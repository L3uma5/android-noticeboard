<?php

define("RESULTS", "results");

//login
define("USERNAME", "username");
define("PASSWORD", "password");

//Users
define("FIRSTNAME", "firstname");
define("FULLNAMES", "full_name");
define("LASTNAME", "lastname");
define("PHONE_NUMBER", "phone_number");
define("EMAIL_ADDRESS", "email");
define("PLACE", "place");

//Place
define("PLACE_ID", "id");
define("PLACE_NAME", "place_name");
define("DESCRIPTION", "description");
define("PLACE_PHONE_NUMBER", "place_phone_number");
define("PLACE_RATINGS", "ratings");
define("LATITUDE", "latitude");
define("LONGITUDE", "longitude");
define("CREATED_AT", "created_at");

//Mpesa
define("SENDER_NAME", "sender_name");
define("SENDER_PHONE_NUMBER", "sender_phone_number");
define("TRANSACTION_CODE", "transaction_code");
define("TRANSACTION_DATE", "transaction_date");
define("TRANSACTION_TIME", "transaction_time");
define("AMOUNT", "amount");
