<?php

include 'Database.php';
include_once 'constants.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $full_names = $_REQUEST[FULLNAMES];
    $phone_number = $_REQUEST[PHONE_NUMBER];
    $email = $_REQUEST[EMAIL_ADDRESS];
    $password = $_REQUEST[PASSWORD];

    $conn = Database::connect();

//    check if the user is already registered first
    $sql_query_reg_id = "select * from users WHERE email='$email'";

    if ($conn->query($sql_query_reg_id)->rowCount() > 0) {
//        user already exists
        echo 2;
    } else {
        $sql = "INSERT INTO users (full_name,email, phone_number, password) VALUES (?,?,?,?)";

        $stmt = $conn->prepare($sql);

        $stmt->bindParam(1, $full_names);
        $stmt->bindParam(2, $email);
        $stmt->bindParam(3, $phone_number);
        $stmt->bindParam(4, $password);

        if ($stmt->execute()) {
//            save success
            echo 0;
        } else {
//            save error
            echo 1;
        }
    }
    Database::disconnect();
} else {
    echo 3;
}
